/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';
import {Button, Platform, StyleSheet, Text, View} from 'react-native';
import BestGameEver  from "./app/GameEngineTest";
import HomeScreen from "./app/screens/homeScreen";
import {createStackNavigator} from 'react-navigation';
import EmptyScreen from "./app/screens/emptyScreen";
import Profile from "./app/screens/profile";
import WordSwipe from './app/screens/wordSwipes';
import TestNewAnimation from "./app/screens/TestNewAnimation";
import FoolsAndKings from "./app/games/FoolsAndKings";
import Statistics from "./app/screens/Statistics";
import MessageScreen from "./app/screens/messageScreen"
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

interface Props {};
export default class App extends React.Component<Props> {
  render() {
    return (
        <RootStack />
    );
  }
}

const RootStack = createStackNavigator(
    {
        Home: HomeScreen,
        Statistics: Profile,
        Team: FoolsAndKings,
        Raid: EmptyScreen,
        WordSwipe: WordSwipe,
        Feedback: MessageScreen,
        Game: WordSwipe,
        Rating: { screen: Statistics, navigationOptions: { tabBarVisible: false } }
    },
    {
        initialRouteName: 'Home',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#8C7CBC',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});