import * as React from "react";
import CountdownCircle from 'react-native-countdown-circle';
import {View} from "react-native";

interface props{
    action: () => void
    id: number
}
interface state {
   id: number
}
export default class RepeatableCountdownCircle extends React.Component<props, state>{
    constructor(props){
        super(props);
        this.state={
            id: 0
        }
    }

    private action(){
        this.props.action();
        this.setState(p => {
           return {id: p.id +1};
        });
    }

    render(){
        console.log("Render");
        console.log(this.state.id);
        return(
            <CountdownCircle
                key={this.state.id}
                seconds={5}
                radius={30}
                borderWidth={8}
                color="#ff003f"
                textStyle={{ fontSize: 20 }}
                onTimeElapsed={() => this.action()}
            />
        );
    }
}
