import * as React from 'react';
import {SceneNavButton, ScreenProps} from "../screens/abstractScreen";
import {View} from "react-native";
var friend = require('../images/friends.png');
var statistics = require('../images/statistics.png');

export default class AdditionalButtonBar extends React.Component<ScreenProps, {}> {
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', width: "100%", marginLeft: 20}}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-around"}}>
                    <SceneNavButton
                        navigation={this.props.navigation}
                        navigationName={"Rating"}
                        image = {require('../images/rating.png')}
                    />
                    <SceneNavButton
                        navigation={this.props.navigation}
                        navigationName={"Statistics"}
                        image = {require('../images/statistics.png')}
                    />
                    <SceneNavButton
                        navigation={this.props.navigation}
                        navigationName={"Team"}
                        image = {require('../images/friends.png')}
                    />
                </View>
            </View>
    );
    }
}
