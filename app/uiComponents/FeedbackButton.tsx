import * as React from 'react';
import {Button, Image, StyleSheet, TouchableOpacity, View, Text} from "react-native";
import {ScreenProps} from "../screens/abstractScreen";

export default class FeedbackButton extends React.Component<ScreenProps, {}> {
    render() {

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Feedback')}}>
                <Image source={require('../images/feedback.png')}/>
            </TouchableOpacity>
        </View>
    );
    }
}

const styles = StyleSheet.create({
    container:{
    flex: 1,
    flexDirection: "column",
    width: "20%",
    height: "20%",
    justifyContent: 'center',
    marginLeft: 40
    },
});
