import * as React from 'react';
import {Button, Image, StyleSheet, TouchableOpacity, View, Text} from "react-native";
import {ScreenProps} from "../screens/abstractScreen";

export default class MainButton extends React.Component<ScreenProps, {}> {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={{justifyContent:'space-around', width: 300}} onPress={()=>{this.props.navigation.navigate('Game')}}>
                    <Image source={require('../images/playButton.png')}
                           style={{width: 300, height: 300, borderRadius: 150}}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: 'center',
        width:"100%",
        height: "100%"
    },
    button: {
        padding: 10,
        marginBottom: 20,
    },
});
