import {Image, StyleSheet, TouchableOpacity} from "react-native";
import * as React from "react";
import backButton from "../images/back.png";

interface props{
    navigation?: Navigator
}

export default class BackButton extends React.Component<props,{}>{
    render(){
       return(
           <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
               <Image style={styles.back} source={backButton} />
           </TouchableOpacity>
       );
    }
}

const styles = StyleSheet.create({
   back:{
           width: 30,
           height: 30,
           justifyContent: 'flex-start',
           margin: 15
   }
})
