import * as React from "react";
import { StyleSheet, StatusBar } from "react-native";
import { GameEngine } from "react-native-game-engine";
import { Finger } from "./renderers/Finger";
import { MoveFinger } from "./systems/MoveFinger"
import HumanCard from "./renderers/HumanCard";

export default class BestGameEver extends React.Component {

    render() {
        return (
            <GameEngine
                style={styles.container}
                systems={[MoveFinger]} //-- We can add as many systems as needed
                entities={{
                    1: { position: [40,  200],height:30,
                        isStatic: true,
                        width:30, id: 1, renderer: <Finger />}, //-- Notice that each entity has a unique id (required)
                    2: { position: [100, 200],height:30,
                        isStatic: false,
                        width:30, id: 2, renderer: <Finger />}, //-- and a map of components. Each entity has an optional
                    3: { position: [160, 300],height:30,
                        isStatic: true,
                        width:30, id: 3, renderer: <Finger />}, //-- renderer component. If no renderer is supplied with the
                    4: { position: [220, 200],height:30,
                        isStatic: true,
                        width:30, id: 4, renderer: <Finger />}, //-- entity - it won't get displayed.
                    5: { position: [280, 200], id: 5,
                        source: require("../assets/img/fjb2.png"),
                        height:30,
                        width:30,
                        renderer: HumanCard},
                    chosen: { id: undefined }
                }}
                key={"15"}
            >

                <StatusBar hidden={true} />

            </GameEngine>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    }
});

// AppRegistry.registerComponent("BestGameEver", () => BestGameEver);