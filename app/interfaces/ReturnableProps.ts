import { DraggableProps } from "./DraggableProps";

export interface ReturnableProps extends DraggableProps {
    startPostition?: Array<number>;
}