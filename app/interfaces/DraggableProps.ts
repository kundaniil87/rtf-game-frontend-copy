import { BaseRendererProps } from "./BaseRendererProps";

export interface DraggableProps extends BaseRendererProps {
    onRelease?: (event, gesture, pan, size) => void
}