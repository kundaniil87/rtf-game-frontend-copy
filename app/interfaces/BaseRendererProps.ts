export interface BaseRendererProps {
    position?: Array<number>;
    width?: number;
    height?: number;
}