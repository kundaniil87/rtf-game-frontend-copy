const delta = 20;

// obj -- слева сверху
function isCaught (x_obj,y_obj,x,y, height, width){
    return ((x > x_obj - delta) &&
            (x < x_obj + width + delta) &&
            (y > y_obj - delta) &&
            (y < y_obj + height + delta));
}

function getNearestEntityId(entities, x, y){

    for (let key in entities) {
        if (entities.hasOwnProperty(key) && ("position" in entities[key])) {
            let position = entities[key].position;
            let height = entities[key].height;
            let width = entities[key].width;

            let isStatic = entities[key].isStatic;

            if (!isStatic && isCaught(x, y, position[0], position[1], height, width)){
                return key;
            }
        }
    }

    return undefined;
}

function move(obj, touch) {
    if (obj && obj.position) {
        obj.position = [
            obj.position[0] + touch.delta.pageX,
            obj.position[1] + touch.delta.pageY
        ];
    }
}

const MoveFinger = (entities, { touches }) => {
    touches.filter(t => t.type === "start").forEach(t => {
        let x_touch = t.event.touches[t.id].pageX;
        let y_touch = t.event.touches[t.id].pageY;
        entities.chosen.id = getNearestEntityId(entities, x_touch, y_touch);
    });

    touches.filter(t => t.type === "move").forEach(t => {
        if (typeof(entities.chosen.id) === "undefined"){
            let x_touch = t.event.touches[t.id].pageX;
            let y_touch = t.event.touches[t.id].pageY;

            entities.chosen.id = getNearestEntityId(entities, x_touch, y_touch);
        }
        if (typeof(entities.chosen.id) !== "undefined"){
            move(entities[entities.chosen.id], t);
        }
    });
    touches.filter(t => t.type === "end").forEach(t => {
        entities.chosen.id = undefined;
    });
    return entities;
};

export { MoveFinger };