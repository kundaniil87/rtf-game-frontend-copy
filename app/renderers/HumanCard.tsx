import * as React from "react";
import { Image, ImageSourcePropType, StyleSheet, View, Text } from "react-native";
import Draggable from "../baseComponents/Draggable";
import { DraggableProps } from "../interfaces/DraggableProps";

export interface Props extends DraggableProps {
    uri?: string;
    source?: ImageSourcePropType;
    label?: string;
}

export interface State {
    isLoaded?: boolean;
}

export default class HumanCard extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        };
    }

    componentDidMount() {
        let {uri} = this.props;
        if (uri) {
            Image.prefetch(uri);
        }
    }

    render() {
        let {width, height, uri, source} = this.props;
        source = source
            ? source
            : {uri};

        return (
            <Draggable {...this.props}>
                <View >
                    <View style={styles.imageContainer}>
                        <Image source={source} style={[{width, height}]}/>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>{this.props.label}</Text>
                    </View>
                </View>
            </Draggable>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    imageContainer: {
        borderRadius: 1,
        borderColor: "#000"
    },
    textContainer: {
        flexDirection: "row",
        justifyContent: "center"
    }
});