import * as React from "react";
import { StyleSheet, View } from "react-native";

const RADIUS = 20;

export interface Props {
    position?: Array<number>;
    id?: string;
}

export interface State {

}

class Finger extends React.Component<Props, State> {
    render() {
        const x = this.props.position[0] - RADIUS / 2;
        const y = this.props.position[1] - RADIUS / 2;
        return (
            <View style={[styles.finger, { left: x, top: y }]} key={this.props.id} />
        );
    }
}

const styles = StyleSheet.create({
    finger: {
        borderColor: "#CCC",
        borderWidth: 4,
        borderRadius: RADIUS * 2,
        width: RADIUS * 2,
        height: RADIUS * 2,
        backgroundColor: "pink",
        position: "absolute"
    }
});

export { Finger };