import * as React from "react";
import { View, StyleSheet, Text } from "react-native";

export interface Props {
    onInit?: (layout) => void;
    label?: string;
}

export interface State {

}

export default class FillingPlace extends React.Component<Props, State> {
    refToComponent: any;

    setDropZone(event){
        let { onInit } = this.props;

        if(this.refToComponent){
            this.refToComponent.measure((x, y, width, height, pageX, pageY)=>{
                if(onInit){
                    let layout = {x, y, width, height, pageX, pageY};
                    onInit(layout);
                }
            });
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.placeHolder]} ref={(ref)=> {this.refToComponent = ref}} onLayout={(event)=> this.setDropZone(event)}>

                </View>
                <View style={styles.labelContainer}>
                    <Text>{this.props.label}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 20,
        paddingRight: 20
    },
    placeHolder: {
        width: 100,
        height: 130,
        borderWidth: 1,
        borderColor: "#000"
    },
    labelContainer: {
        flexDirection: "row",
        justifyContent: "center"
    }
});