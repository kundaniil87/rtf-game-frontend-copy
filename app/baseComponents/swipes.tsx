import React, { Component } from "react";
import Swiper from 'react-native-deck-swiper'
import { StyleSheet, Text, View } from 'react-native'
import RepeatableCountdownCircle from "../uiComponents/RepeatableCountdownCircle.tsx";

export default class Exemple extends Component {
  constructor(props) {
    super(props)
    this.state = {
      swipedAllCards: false,
      swipeDirection: '',
      isSwipingBack: false,
      cardIndex: 0,
      id: 0
    }
  }

  renderCard = (card, index) => {
    return (
      <View style={styles.card}>
        <Text style={styles.text}>{card}</Text>
      </View>
    )
  };

  onSwipedAllCards = () => {
    this.props.onDone(this.state.values)
    this.setState({
      swipedAllCards: true
    })
  };

  swipeBack = () => {
    if (!this.state.isSwipingBack) {
      this.setIsSwipingBack(true, () => {
        this.swiper.swipeBack(() => {
          this.setIsSwipingBack(false)
        })
      })
    }
  };

  setIsSwipingBack = (isSwipingBack, cb) => {
    this.setState(
      {
        isSwipingBack: isSwipingBack
      },
      cb
    )
  };

  swipeLeft = () => {
    this.swiper.swipeLeft()
  };

  swipeBottom = () => {
    this.swiper.swipeBottom()
  }

  swipe = (value, i) => {
    const values = this.state.values || this.props.cards.map(() => null)
    this.setState({
      values: values.map((v, I) => i===I ? value : v),
    })
      this.setState(p => {
          return {id: p.id +1};
      });
  }

  render() {
    return (
      <View style={styles.container} >
        <View flex={1} style={styles.countDown}>
          <RepeatableCountdownCircle key ={this.state.id} action={this.swipeBottom}/>
        </View>
        <View flex={6}>
      <Swiper
        ref={swiper => {
          this.swiper = swiper
        }}
        backgroundColor="#E9E9EF"
        onSwiped={this.onSwiped}
        onSwipedTop={null}
        onSwipedBottom={null}
        onSwipedLeft={i => this.swipe("No", i)}
        onSwipedRight={i => this.swipe("Yes", i)}
        onTapCard={this.swipeLeft}
        cards={this.props.cards}
        cardIndex={this.state.cardIndex}
        cardStyle={this.props.cardStyle || {}}
        cardVerticalMargin={0}
        renderCard={this.renderCard}
        onSwipedAll={this.onSwipedAllCards}
        stackSize={3}
        stackSeparation={15}
        verticalSwipe={true}
        disableBottomSwipe={true}
        overlayLabels={{
          left: {
            title: 'Нет',
            style: {
              label: {
                backgroundColor: 'red',
                borderColor: 'black',
                color: 'white',
                borderWidth: 1
              },
              wrapper: {
                flexDirection: 'column',
                alignItems: 'flex-end',
                justifyContent: 'flex-start',
                marginTop: 30,
                marginLeft: -30
              }
            }
          },
          right: {
            title: 'Да',
            style: {
              label: {
                backgroundColor: 'green',
                borderColor: 'black',
                color: 'white',
                borderWidth: 1
              },
              wrapper: {
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
                marginTop: 30,
                marginLeft: 30
              }
            }
          },
        }}
        animateOverlayLabelsOpacity
        animateCardOpacity
      >
      </Swiper>
      </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    countDown: {
        backgroundColor: "#E9E9EF",
        padding: 10
    },
  container: {
    flex: 1,
      flexDirection: 'column',
    backgroundColor: '#F5FCFF'
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent'
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent'
  }
})
