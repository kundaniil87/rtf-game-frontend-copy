import * as React from "react";
import {View, StyleSheet, Animated, PanResponder, Dimensions} from "react-native";

import { DraggableProps } from "../interfaces/DraggableProps";


export interface State {
    pan?: any;
}
let Window = Dimensions.get("window");
export default class Draggable extends React.Component<DraggableProps, State> {
    panResponder: any;

    constructor(props){
        super(props);

        this.state = {
            pan: new Animated.ValueXY()
        };
        let {width, height} = props;
        let size = {width, height};
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: ()=> true,
            onMoveShouldSetPanResponder: () => true,
            onPanResponderMove: Animated.event([null, {
                dx: this.state.pan.x,
                dy: this.state.pan.y
            }]),
            onPanResponderRelease: (e, gesture)=> {
                let { onRelease } = this.props;

                if(onRelease) {
                    onRelease(e, gesture, this.state.pan, size);
                }
            }
        });
    }

    componentDidMount(){

    }

    render() {
        let { width, height } = this.props;
        return (
            <Animated.View {...this.panResponder.panHandlers}
                            style={[style.draggable, this.state.pan.getLayout(), {width, height}]}>
                {this.props.children}
            </Animated.View>
        );
    }
}

const style = StyleSheet.create({
    draggable: {

    }
});