import React, {Component} from "react";
import {Bar} from 'react-native-progress';
import { View, Text } from "react-native";
import ellipsize from 'ellipsize';

export default class Achievement extends Component {
  render() {
    const MAX_CHARS = 100;
    const small = ellipsize(this.props.title, MAX_CHARS - 3);
    const title = (this.props.title.length > MAX_CHARS) ? `${small}...` : this.props.title;
    const progress = +this.props.progress;
    return (
      <View style={{flex: 1, flexDirection: 'row', margin: 10}}>
        <View flex={5} style={{height: 20}}>
          <Text>{title}</Text>
        </View>
        <View flex={5} style={{height: 20, marginTop:7}}>
          <Bar progress={(progress)} size={300} color="#8C7CBC" />
        </View>
        <View flex={1} style={{height: 20}} >
          <Text>{`${(progress) * 100}%`}</Text>
        </View>
      </View>
    )
  }
}
