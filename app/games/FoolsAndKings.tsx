import * as React from "react";
import { View, StyleSheet, Animated } from "react-native";
import FillingPlace from "../renderers/FillingPlace";
import HumanCard from "../renderers/HumanCard";
import BackButton from "../uiComponents/BackButton";
import {ScreenProps} from "../screens/abstractScreen";

export interface Props extends ScreenProps{

}

export interface State {

}

// let fillingPlaces = [
//     {
//         id: 0,
//         label: "Король",
//         renderer: FillingPlace,
//         layout: undefined,
//         relative: undefined
//     },
//     {
//         id: 1,
//         label: "Защитник короля",
//         renderer: FillingPlace,
//         layout: undefined,
//         relative: undefined
//     },
//     {
//         id: 2,
//         label: "Шут",
//         renderer: FillingPlace,
//         layout: undefined,
//         relative: undefined
//     }
// ];


export default class FoolsAndKings extends React.Component<Props, State> {
    fillingPlaces: any;
    peoples: any;

    constructor(props){
        super(props);

        this.fillingPlaces = [
            {
                id: 0,
                label: "Король",
                renderer: FillingPlace,
                layout: undefined,
                relative: undefined
            },
            {
                id: 1,
                label: "Защитник короля",
                renderer: FillingPlace,
                layout: undefined,
                relative: undefined
            },
            {
                id: 2,
                label: "Шут",
                renderer: FillingPlace,
                layout: undefined,
                relative: undefined
            }
        ];
        this.peoples = [
            {
                id: 0,
                renderer: HumanCard,
                label: "Иван",
                source: require("../images/peoples/alex.jpg")
            },
            {
                id: 1,
                renderer: HumanCard,
                label: "Василий",
                source: require("../images/peoples/ded.jpg")
            },
            {
                id: 2,
                renderer: HumanCard,
                label: "Алексей",
                source: require("../images/peoples/oleg.jpg")
            }
        ];
    }

    componentDidMount(){
       this.resetFilledPlaces()
    }
    static navigationOptions = ({navigation}) => {
        return{
            title: 'Короли и Шуты',
            headerLeft:(<BackButton navigation={navigation}/>)
        }
    };

    resetFilledPlaces(){
       this.fillingPlaces.forEach(t=>{
           t.relative = undefined;
       })
    }

    onInitHandler(layout, index){
        this.fillingPlaces[index].layout = layout;
    }

    onFinishHandler(){
       if(this.allPlacesFilled())
           this.props.navigation.goBack();
    }

    allPlacesFilled(){
        var i = 0;
        this.fillingPlaces.forEach(t=>{
           if(t.relative !== undefined)
               i = i + 1;
        })
        if(i == this.fillingPlaces.length)
            return true;
        else
            return false;
    }

    onRelease(event, gesture, pan, size, index){
        let filledPlace = this.fillingPlaces.find((fillingPlace)=>this.isDropped(fillingPlace, gesture));
        if (filledPlace){
            filledPlace.relative = index;
        this.onFinishHandler();
        }else {
            Animated.spring(pan, {toValue: {x: 0, y: 0}}).start();
        }
    }

    isDropped(fillingPlace, gesture){
        let {layout} = fillingPlace;
        let { moveX, moveY } = gesture;
        debugger;
        return ((moveX >= layout.pageX)&&(moveX <= layout.pageX + layout.width))
            &&((moveY >= layout.pageY)&&(moveY <= layout.pageY + layout.height));
    }

    renderPlaces() {
        return this.fillingPlaces.map((item, index)=>{
           if(item.renderer){
                return <item.renderer key={"fillingPlaces " + index} onInit={(layout)=>this.onInitHandler(layout, index)} {...item}/>
            }
        });
    }

    renderPeople() {
        return this.peoples.map((item, index)=>{
            return <item.renderer key={"peoples " + index} width={80} height={100} onRelease={(event, gesture, pan, size)=> this.onRelease(event, gesture, pan, size, index)} {...item}/>
        });
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.placesContainer}>
                    {this.renderPlaces()}
                </View>
                <View style={styles.peopleContainer}>
                    {this.renderPeople()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        paddingTop: 20
    },
    placesContainer: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-around",
        alignContent: "space-between"
    },
    peopleContainer: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "nowrap",
        justifyContent: "space-around",
        paddingTop: 120
    }
});