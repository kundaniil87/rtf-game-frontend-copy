import * as React from "react";
import {View, Text} from "react-native";
import { createStackNavigator } from 'react-navigation';
import {ScreenProps} from "./abstractScreen";
import BackButton from "../uiComponents/BackButton";


export default class EmptyScreen extends React.Component<ScreenProps, {}> {

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (<BackButton navigation={navigation}/>)
        }
    }
    render(){
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Empty Screen</Text>
        </View>
    );
    }
}