import React, {Component} from 'react';
import Swipes from '../baseComponents/swipes';
import {View, Text} from 'react-native';
import {getFileData} from "../utils/fileUtils.js";
import BackButton from "../uiComponents/BackButton";


export default class WordSwipes extends Component {
  state = {
    done: false,
  }
    static navigationOptions = ({navigation}) => {
        return{
            title: 'Оцени Иванова Ивана',
            headerLeft:(<BackButton navigation={navigation}/>)
        }
    };
  render() {
    if (this.state.done) {
      return <View><Text>`Ты проотвечался: ${JSON.stringify(this.state.answers)}`</Text></View>
    }
    var a = require('../../assets/data/swipeData.json');
    console.log(a);
    const words = a.words;
    console.log(words);
    const onDone = (answers) => {
      this.setState({answers, done: true})
    }
    return (
      <View style={{flex: 1}} >
        <View flex={8}>
          <Swipes cards={words} onDone={onDone} cardStyle={{height: 400}}/>
        </View>
      </View>
    )
  }
}
