import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import CompleteFlatList from 'react-native-complete-flatlist';
import Achievement from '../baseComponents/Achievement'
import BackButton from "../uiComponents/BackButton";
import {ScreenProps} from "./abstractScreen";

export default class Profile extends Component<ScreenProps,{}>{
        static navigationOptions = ({navigation}) => {
            return{
                title: 'Васильченко Петр',
                headerLeft:(<BackButton navigation={navigation}/>)
            }
    };
  prepareData(data) {
    return data.map(a=>({...a, progress: (""+a.progress)}))
  }
  render() {
    try {
      //const profile = this.props.profile
      //const data = this.props.data;
      const profile = {
        title: "Рефакторинг гуру",
        image: require('../images/fisk.png'),
        tagcloud: require("../images/tagcloud.png")
      };

      const data = [
        {title: 'Коммуникабельность', progress: 0.44},
        {title: 'Умение работать в команде', progress: 0.3},
        {title: 'Пунктуальность', progress: 0.9},
        {title: 'Быстрая обучаемость', progress: 0.1},
        {title: 'Креативность', progress: 0.2},
      ];
      return (
        <View style={{flex: 1}} >
          <View style={{flexDirection: 'row'}} >
            <View flex={3}>
              <Text style={{fontSize: 20, fontWeight: 'bold'}}>{profile.name}</Text>
              <Image style={{width: 100, height: 100, borderRadius:100, margin: 10}} source={profile.image} />
            </View>
            <View flex={7.5} style={{padding: 20}}>
              <Image style={{width: 280, height: 170}} source={profile.tagcloud}/>
            </View>
          </View>
          <CompleteFlatList
          searchKey={['title', 'progress']}
          highlightColor="yellow"
          pullToRefreshCallback={() => {
          }}
          data={this.prepareData(data)}
          renderSeparator={null}
          renderItem={(props, i)=>{return <Achievement title={data[i].title} progress={data[i].progress} />}}
        />
        </View>
      );
    } catch(e) {
      return <View><Text>{e.message}</Text></View>
    }
  }
}
