import * as React from "react";
import {View, Text, Button} from "react-native";
import { createStackNavigator } from 'react-navigation';
import {Navigator} from 'react-navigation';
import {ScreenProps} from "./abstractScreen";
import {SceneNavButton} from "./abstractScreen";
import LogoTitle from "../uiComponents/LogoTitle";
import AdditionalButtonBar from "../uiComponents/AdditionalButtonBar";
import MainButton from "../uiComponents/MainButton";
import FeedbackButton from "../uiComponents/FeedbackButton";


export default class HomeScreen extends React.Component<ScreenProps, {}> {
    static navigationOptions = { // headerTitle instead of title
        header: null
    };
    render() {
        return(
        <View style={{flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor:"#E9E9EF"}}>
            <View>
                <View flex={1}>
                    <FeedbackButton {... this.props}/>
                </View>
                <View flex={5} style={{marginBottom:20}}>
                    <MainButton navigation={this.props.navigation} />
                </View>
                <View flex={1} style={{width:"100%"}}>
                    <AdditionalButtonBar navigation={this.props.navigation}/>
                </View>
            </View>
        </View>

        )
    }
}
