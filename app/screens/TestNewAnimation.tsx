import * as React from "react";
import {
    View,
    StyleSheet,
    Animated
} from "react-native";
import HumanCard from "../renderers/HumanCard";

export interface Props {

}

export interface State {
    dropZoneValues?: any;
    draggableVisible?: boolean;
}

export default class TestNewAnimation extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            draggableVisible: true
        };
    }

    setDroZoneValues(event) {
        this.setState({
            dropZoneValues: event.nativeEvent.layout
        });
    }

    handleOnRelease(event, gesture, pan, size){
        if(this.isDropZone(pan,size)) {
            this.setState({draggableVisible: false})
        } else {
            Animated.spring(pan, {toValue:{x:0, y:0}}).start();
        }
    }

    isDropZone(pan, size): boolean{
        let dz = this.state.dropZoneValues;
        debugger;
        return pan.y._value + size.height > dz.y && pan.y._value + size.height < dz.y + dz.height;
    }

    render() {
        return (
            <View>
                <View
                    style={[styles.dropZone]}
                    onLayout={(event) => this.setDroZoneValues(event)}
                />
                {this.state.draggableVisible &&
                <HumanCard
                    source={require("../../assets/img/Button_Play.png")}
                    width={64}
                    height={64}
                    onRelease={(event, gesture, pan, size) => this.handleOnRelease(event, gesture, pan, size)}/>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    dropZone: {
        width: "100%",
        height: 100,
        backgroundColor: "#FF0000"
    }
})