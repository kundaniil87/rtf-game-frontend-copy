import React,
       { Component } from 'react';
import { View,
         Text,
         StyleSheet,
         ScrollView,
         Image,
         TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
import { ScreenProps } from "./abstractScreen";
import BackButton from "../uiComponents/BackButton";

interface Props extends ScreenProps{

}

interface Top{
    name: string;
    items: string[];
}

interface States{
    TopLists: Top[];
}

class Dot extends Component{
    render(){
        return(
            <View  style={styles.dot} />
        )
    }
}

class Statistics extends Component<Props, States> {
    static navigationOptions = { header: null }


    constructor(props) {
        super(props);
        this.state = {
            TopLists: [
                {
                    name: "Корпоративный дух",
                    items: [
                        "Иванов Иван Иванович",
                        "Потапенко Николай Сергеевич",
                        "Ганс Христиан Андерсон",
                        "Бродский Иосиф Александрович",
                        "Смирнов Николай Васильевич"
                    ]
                },
                {
                    name: "Креативность",
                    items: [
                        "Кундик Даниил Викторович",
                        "Семёнов Семенович Семён",
                        "Иванов Иван Иванович",
                        "Потапенко Потапоп Потапович",
                        "Антонов Антон Антонович"
                    ]
                },
                {
                    name: "Мисс Пунктуальность",
                    items:[
                        "Озерчук Мария Михайловна",
                        "Старикова Анна Дмитриевна",
                        "Заглушка Антонина Загитовна",
                        "Семенчук Ангелина Демеидовна",
                        "Галииева Жанна Святославовна"
                    ]
                },
                {
                    name: "Чистый код",
                    items: [
                        "Хакин Владислав Александрович",
                        "Ёлкина Ольга Викторовна",
                        "Ершов Станислав Ерышович",
                        "Ракитин Виталий Павлович",
                        "Смирнов Семён Семёнович"
                    ]
                }
            ],
        };
    }

    render(){

        let statistics = this.state.TopLists.map((listItem, itemIndex)=> {
            let list = listItem.items.map((item, index) => (
                <View style={{margin: 10}}>
                    <Text style={styles.text}>
                        {(index + 1).toString() + "." + " " + item}
                    </Text>
                </View>
                ));

            return(

                <View flexDirection={"column"} style = {itemIndex % 2 ? styles.slide_1 : styles.slide_2}>
                    <BackButton navigation={this.props.navigation}/>
                    <View style={styles.headerView}>
                        <Text style={styles.header}>
                            {"Tоп 5. " + listItem.name}
                        </Text>
                    </View>
                    <ScrollView >
                        {list}
                    </ScrollView>
                </View>
            )
        });

        return (
            <Swiper
                    activeDot={(<Dot />)}
                    showsButtons={false}
                    loop={false}
            >
                {statistics}
            </Swiper>
        );
    }
}

const styles = StyleSheet.create({
    dot: {
        backgroundColor: '#fff',
        width: 13,
        height: 13,
        borderRadius: 7,
        marginLeft: 7,
        marginRight: 7
    },
    slide_1: {
        flex: 1,
        backgroundColor: '#9C8CC6',
    },
    slide_2: {
        flex: 1,
        backgroundColor: '#8C7CBC',
    },
    header: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
    },
    headerView:{
        marginTop: 10,
        marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    back:{
        width: 30,
        height: 30,
        justifyContent: 'flex-start',
        margin: 15
    },
    text: {
        color: '#FFFFFF',
        fontSize: 19,
        alignContent: 'flex-start'
    }
});

export default Statistics;