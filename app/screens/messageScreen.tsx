import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Button} from 'react-native';
import BackButton from "../uiComponents/BackButton";
import {ScreenProps} from "./abstractScreen";


interface Props extends ScreenProps {
}

interface State {
    text: string;
}

export default class MessageScreen extends Component<Props, State> {
    constructor(props){
        super(props);

        this.state = {
            text: ""
        }
    }
    static navigationOptions = ({navigation}) => {
        return{
            title: 'Рекомендация Потапову П.П.',
            headerLeft:(<BackButton navigation={navigation}/>)
        }
    };
    render() {
        return (
        <View>
            <Text style = {{margin: 15}}>
                Последнее время мы стали отмечать, что у Вашего коллеги Потапова Потапа Потаповича стали снижаться показатели по профессиональным качествам. Дайте пару советов, как ему лучше работать над собой.
            </Text>

            <View style={styles.inputForm}
            >
                <TextInput
                    multiline = {true}
                    numberOfLines = {4}
                    placeholder={"Дорогой, Потап! ..."}
                    placeholderTextColor={'#8C7CBC'}
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                    style={styles.text}
                    scrollEnabled={true}
                />
            </View>
            <View style ={{margin:20}}>
                <Button
                    title={"Отправить"}
                    color={'#8C7CBC'}
                    onPress={()=>{this.props.navigation.goBack()}}
                />
            </View>
        </View>
        )
    }
}



const styles = StyleSheet.create({
    inputForm: {
        borderRadius: 4,
        borderWidth: 2,
        margin: 20,
        height: 300,
        borderColor: '#E8E8E8',
        backgroundColor: 'white'
    },
    text: {
        textAlign: 'left',
        fontSize: 14,
    }
});
