import * as React from 'react';
import {Navigator} from "react-navigation";
import {View, Text, Button, TouchableOpacity, Image, StyleSheet, ImageSourcePropType} from "react-native";

export interface ScreenProps{
    navigation?: Navigator
}

interface SceneButtonProps {
    title?: string,
    navigationName?: string,
    image?: ImageSourcePropType
    navigation: Navigator
}

export class SceneNavButton extends React.Component<SceneButtonProps,{}>{
    render(){
       return(
           <View>
               <TouchableOpacity style={styles.button} onPress={()=>{this.props.navigation.navigate(this.props.navigationName)}}>
                   <Image
                       source={this.props.image}
                       style={{ width: 30, height: 30 }}
                   />
               </TouchableOpacity>
               {this.props.title == "" ? null :
                   <Text>{this.props.title}</Text>}
           </View>
       )
    }
}
const styles = StyleSheet.create({
    button: {
        backgroundColor: '#97BFE5',
        borderRadius: 20,
        padding: 10,
        marginBottom: 20,
        shadowColor: '#303838',
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 10,
        shadowOpacity: 0.35,
        width: 50
    },
});
